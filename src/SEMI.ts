(() => {
    // Scripting Engine for Melvor Idle by aldousWatts on GitLab
    // Major code & script contributions by DanielRX & many community code contributors!
    // As always, use and modify at your own risk. But hey, contribute and share!
    // This code is open source and shared freely under MPL/GNUv3/creative commons licenses.

    //Browser detection cases
    const isChrome = navigator.userAgent.match('Chrome');
    const isFirefox = navigator.userAgent.match('Firefox');

    // Only support Firefox and Chrome. To allow loading on other browsers, delete this entire if statement including brackets & contents {}.
    if (!isChrome && !isFirefox) {
        return alert(
            'SEMI is only officially supported on Firefox and Chrome. To try on another browser, you must modify the main function in SEMI.js. The addon will not load otherwise.'
        );
    }

    const getRuntime = () => (isChrome ? chrome : browser).runtime;

    /**
     * Get extension resource URL
     * @param {string} name
     */
    const getURL = (name) => getRuntime().getURL(name);

    /**
     * Check if an element exists
     * @param {string} id
     */
    const exists = (id) => document.contains(document.getElementById(id));

    /**
     * Inject a script into the page
     * @param {string} name
     * @param {string} scriptID
     */
    const addScript = (name, scriptID, async = false) => {
        const script = document.createElement('script');
        script.src = getURL(name);
        script.setAttribute('id', scriptID);

        if (async) {
            script.setAttribute('async', 'true');
        }

        document.body.appendChild(script);
    };

    /**
     * Inject a short script that creates a global constant with SEMI's current version
     * @param {string} version
     */
    const addSemiVersion = (version) => {
        const script = document.createElement('script');
        script.setAttribute('id', 'semiVersion');
        script.innerText = `const SEMI_VERSION='${version}';`;
        document.body.prepend(script);
    };

    /**
     * Inject a script, removing and replacing it if it already existed on the page
     * @param {string} name
     * @param {string} scriptID
     */
    const replaceScript = (name, scriptID, async = false) => {
        const el = document.getElementById(scriptID);
        if (exists(scriptID)) {
            el.remove();
        }
        addScript(name, scriptID, async);
    };

    /** @param {string} name */
    const addPlugin = (name) => {
        replaceScript(`scripts/plugins/${name}.js`, `SEMI-${name}`, true);
    };

    /** @param {string} name */
    const addSemiLib = (name) => {
        replaceScript(`scripts/semi/${name}.js`, `SEMI-${name}`);
    };

    /**
     * Create and return image element with height & width of 32px
     * @param {string} url
     * @param {string} imgId
     */
    const createImage = (url, imgId) => {
        const img = document.createElement('img');
        img.src = getURL(url);
        img.id = imgId;
        img.height = 32;
        img.width = 32;
        return img;
    };

    //Check if SEMI is already loaded, and if so, let user know and stop trying to load.
    if (exists('semiVersion')) {
        return alert(
            'SEMI just tried to load, but found that SEMI already exists on the page. This may mean your browser automatically updated the extension and you need to refresh to finish the update!'
        );
    }

    //Mapping script names for later injection
    const autoNames = [
        'bonfire',
        'mine',
        'runecraft',
        'agility',
        'sell-gems',
        'master',
        'smith',
        'sell',
        'slayer',
        'slayer-skip',
        'open',
        'bury',
        'equip',
        'loot',
        'farm',
        // 'equip-best-items',
        'lute',
        'eat',
        'run',
    ];
    const pluginNames = [
        ...autoNames.map((name) => `auto-${name}`),
        'mastery-enhancements',
        'eta',
        'drop-chances',
        'ore-in-bank',
        'calc-to-level',
        'destroy-crops',
        'xp-per-hour',
        'save-on-close',
        'sort-override',
        'living-bank-helper',
    ];
    const libNames = ['fold-menus', 'drag-menus', 'menus'];
    const preloadedNames = ['event-bus', 'settings-migrator', 'injections'];

    // Verifies that a character is selected before allowing SEMI to load
    const checkLoaded = async () => {
        const accountName = document.getElementById('account-name');

        if (!accountName) {
            // Page isn't loaded, silently fail
            return;
        }

        // User hasn't selected their account yet
        const isCharacterSelected = accountName.innerText.length > 0;
        if (!isCharacterSelected) {
            return;
        }
        clearInterval(preLoader);

        //Load and inject SEMI
        const navbar = document.getElementsByClassName('nav-main')[0];
        const semiVersion = getRuntime().getManifest().version;
        addSemiVersion(semiVersion);
        const semiHeading = document.createElement('li');
        semiHeading.className = 'nav-main-heading';
        navbar.appendChild(semiHeading);
        semiHeading.style.fontSize = '12pt';
        semiHeading.style.color = 'white';
        semiHeading.style.textTransform = 'none';
        semiHeading.textContent = ` SEMI v${semiVersion}`;
        semiHeading.title = 'Scripting Engine for Melvor Idle';
        semiHeading.id = 'SEMI-heading';
        semiHeading.insertBefore(createImage('icons/border-48.png', 'SEMI-menu-icon'), semiHeading.childNodes[0]);

        const coreInjectionPromise = new Promise((resolve, reject) => {
            replaceScript('scripts/core.js', 'semi-inject-core');
            setTimeout(() => resolve(true), 250);
        });
        coreInjectionPromise.then((res) => (res !== true ? console.error('BADS!') : console.log('good')));
        replaceScript('scripts/utils.js', 'semi-inject-utils');

        // These items need to load with Melvor, not SEMI, but require Core functionality.
        preloadedNames.forEach(addSemiLib);
    };
    const preLoader = setInterval(checkLoaded, 500);

    const loadingTimer = (fn, ms) => {
        return (pluginName, i) => {
            setTimeout(() => {
                fn(pluginName);
            }, i * ms);
        };
    };

    const loadPlugins = () => {
        if (!exists('SEMI-canary')) {
            return;
        }
        clearInterval(pluginLoader);

        // Load Libs first asynchronously so that they can depend on each other.
        libNames.forEach(addSemiLib);

        // Load Plugins async
        pluginNames.forEach(loadingTimer(addPlugin, 1));
    };
    const pluginLoader = setInterval(loadPlugins, 50);
})();
