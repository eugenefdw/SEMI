(() => {
    const id = 'auto-smith-bars';
    const title = 'AutoSmith Bars';
    const desc = 'AutoSmith Bars will cycle through your smithing bars and smelt those you have materials for.';
    const imgSrc = 'assets/media/bank/dragonite_bar.png';
    const skill = 'Smithing';

    const config = {
        ignoreIron: false,
    };

    //Auto Smelt
    const bars = smithingSelectionTabs[0].recipes;

    let barTypeCount = 0;
    const findBarIndex = (bar) => this.smithingItems.findIndex((y) => y.smithingID === bar.smithingID);
    const getBarTypes = () => {
        barTypes = bars.map(findBarIndex).sort();
    };

    let barTypes = [];
    const getItemId = () => this.smithingItems[barTypes[barTypeCount]].itemID;
    const moveToNext = () => {
        barTypeCount = (barTypeCount + 1) % barTypes.length;
        selectSmith(barTypes[barTypeCount]);
    };

    const autoSmithBars = () => {
        // Forces the system to open to the tab I want
        smithingSelectionTabs.forEach((tab) => {
            tab.hide();
        });

        smithingSelectionTabs[0].show();

        const ignoringIron =
            SEMI.getValue(id, 'ignoreIron') && this.smithingItems[barTypes[barTypeCount]].name === 'Iron Bar';

        // If we haven't selected a recipe, selects Bronze Bar by default
        if (smithingArtisanMenu.noneSelected) {
            selectSmith(0);
        }

        // Iterates through bars, making sure we can smith them and ensuring that if we can't we move to the next one
        for (const _ in barTypes) {
            if (!checkSmithingReq(getItemId()) || ignoringIron) {
                moveToNext();
            }
        }

        if (!SEMIUtils.isCurrentSkill(skill)) {
            startSmithing(true);
        }
    };

    const onDisable = () => {
        SEMIUtils.stopSkill(skill);
    };

    const hasConfig = true;
    const configMenu = `<div class="form-group">
        <div class="custom-control custom-switch mb-1">
            <input type="checkbox" class="custom-control-input" id="${id}-iron-toggle" name="${id}-iron-toggle">
            <label class="custom-control-label" for="${id}-iron-toggle">
                Ignore Iron (for Steel smelting)
            </label>
        </div>
    </div>`;
    const saveConfig = () => {
        const toggled = $(`#${id}-iron-toggle`).prop('checked');
        SEMI.setValue(id, 'ignoreIron', toggled);
        SEMI.setItem(`${id}-config`, SEMI.getValues(id));
        SEMIUtils.customNotify(imgSrc, 'AutoSmith Bars config saved!');
    };
    const updateConfig = () => {
        $(`#${id}-iron-toggle`).prop('checked', SEMI.getValue(id, 'ignoreIron'));
    };

    SEMI.add(id, {
        onLoop: autoSmithBars,
        onEnable: getBarTypes,
        onDisable,
        config,
        hasConfig,
        configMenu,
        saveConfig,
        updateConfig,
        title,
        desc,
        imgSrc,
        skill,
    });
})();
