const SEMIUtils = (() => {
    /** @typedef {keyof typeof CONSTANTS.skill} SkillName */

    /** @param {string} id */
    const getElement = (id) => {
        return $(`#${globalThis.SEMI.ROOT_ID}-${id}`).first();
    };

    /** @param {string} id */
    const getElements = (id) => {
        return id == '' ? $(`[id^=${globalThis.SEMI.ROOT_ID}]`) : $(`[id^=${globalThis.SEMI.ROOT_ID}-${id}]`);
    };
    const iconSrc = getElement('icon').attr('src');

    /**
     * Custom notifications! Green background with text, custom image and SEMI icon, variable display time.
     * @param {string} imgsrc
     * @param {string} msg
     * @param {Object} opts
     * @param {number} opts.duration milliseconds that the notification will display
     * @param {boolean} opts.lowPriority skip this notification if lessNotification is enabled in SEMI options
     */
    const customNotify = (imgsrc = '', msg = 'Custom Notifications!', opts: any = {}) => {
        opts = Object.assign(
            {},
            {
                duration: 3000,
                lowPriority: false,
            },
            opts
        );
        const lessNotifications = globalThis.SEMI.getItem('etc-GUI-toggles').lessNotifications;
        if (lessNotifications && opts.lowPriority) {
            return;
        }
        Toastify({
            text: `<div class="text-center"><img class="notification-img" src="${imgsrc}"><img src="${iconSrc}" height="auto" width="auto" style="margin: 4px;"><span class="badge badge-success"> ${msg} </span></div>`,
            duration: opts.duration,
            gravity: 'bottom', // `top` or `bottom`
            position: 'center', // `left`, `center` or `right`
            backgroundColor: 'transparent',
            stopOnFocus: false,
        }).showToast();
    };

    /**
     * @param {HTMLElement} x
     * @param {object} y
     */
    const mergeOnto = (x, y) => {
        Object.keys(y).forEach((key) => {
            x[key] = y[key];
        });
    };
    //the above func is also included in core.js

    /** @param {number} id */
    const getBankQty = (id) => {
        const itemBankID = SEMIUtils.getBankId(id);

        if (itemBankID !== false) {
            return bank[itemBankID].qty;
        }
        return 0;
    };

    /**
     * @param {number} id
     * @returns {number | boolean} the index of the item in the bank, or false if the item is not in the bank
     */
    const getBankId = (itemId) => {
        const id = globalThis.getBankId(itemId);
        if (id === -1) {
            return false;
        }
        return id;
    };

    //may want to refactor all checkBankForItem() to a SEMI func to make it easier to fix if changed.
    /** @param {number?} itemID */
    const equipFromBank = (itemID, qty = 1) => {
        if (typeof itemID === 'undefined' || itemID === 0 || !checkBankForItem(itemID)) {
            return false;
        }
        player.equipItem(itemID, player.selectedEquipmentSet, undefined, qty);
        return true;
    };

    const currentEquipment = () => player.equipment.slots;

    /** Wrapper function for getting item ID of current equipment by slot name
     * @param {string} slotName
     * @returns {number} item ID of equipment, or -1 if none
     * */
    const currentEquipmentInSlot = (slotName) => {
        if (!Object.keys(player.equipment.slots).includes(slotName)) return;
        return player.equipment.slots[slotName].item.id;
    };

    const isBankFull = () => {
        return bank.length >= getMaxBankSpace();
    };

    const unselectItemIfNotInBank = (itemID) => {
        if ((selectedBankItem === itemID || itemsToSell.includes(itemID)) && !checkBankForItem(itemID)) {
            if (selectedBankItem === itemID) {
                deselectBankItem();
            }
            if (itemsToSell.includes(itemID)) {
                addItemToItemSaleArray(itemID);
            }
        }
    };

    const sellItemWithoutConfirmation = (itemID, qty = 1) => {
        if (!checkBankForItem(itemID)) {
            return false;
        }

        let saleModifier = 1; // Need to determine saleModifier because for some reason this isn't done inside processItemSale()
        if (
            items[itemID].type === 'Logs' &&
            getMasteryPoolProgress(CONSTANTS.skill.Woodcutting) >= masteryCheckpoints[2]
        ) {
            saleModifier += 0.5;
        }

        const _selectBankItem = selectBankItem;
        // @ts-expect-error
        selectBankItem = () => {}; // Temporarily replace selectBankItem() because this can cause errors when it gets called in processItemSale()
        try {
            processItemSale(itemID, qty, saleModifier);
        } catch (e) {
            console.error(`SEMI: Unable to sell ${items[itemID].name} due to an error in processItemSale():`);
            console.error(e);
            return false;
        } finally {
            // @ts-expect-error
            selectBankItem = _selectBankItem;
            unselectItemIfNotInBank(itemID);
        }
        return true;
    };

    function processItemSaleWithoutBank(itemID?, qty?) {
        let saleModifier = 1;
        if (
            items[itemID].type === 'Logs' &&
            getMasteryPoolProgress(CONSTANTS.skill.Woodcutting) >= masteryCheckpoints[2]
        ) {
            saleModifier += 0.5;
        }

        let saleAmount = Math.floor(items[itemID].sellsFor * qty * saleModifier);
        //Add gp for the sale
        gp += saleAmount;
        //Update respective stats
        if (items[itemID].category === 'Woodcutting') {
            statsWoodcutting[1].count += qty;
            statsWoodcutting[2].count += saleAmount;
            updateStats('woodcutting');
        }
        //Update respective stats
        if (items[itemID].category === 'Fishing') {
            statsFishing[3].count += qty;
            statsFishing[4].count += saleAmount;
            updateStats('fishing');
        }
        statsGeneral[0].count += saleAmount;
        statsGeneral[1].count += qty;
        updateStats('general');

        // TODO: are these wrong?? itemStats[i] is an array, not an object
        // @ts-expect-error
        itemStats[itemID].timesSold += qty;
        // @ts-expect-error
        itemStats[itemID].gpFromSale += saleAmount;

        updateGP();
        //saveData();
    }

    const buryItemWithoutConfirmation = (itemID, qty = 1) => {
        if (!checkBankForItem(itemID)) {
            return false;
        }

        const _selectedBankItem = selectedBankItem;
        selectedBankItem = itemID;
        const _buryItemQty = buryItemQty;
        buryItemQty = qty;
        const _selectBankItem = selectBankItem;
        // @ts-expect-error
        selectBankItem = () => {}; // Temporarily replace selectBankItem() because this can cause errors when it gets called in buryItem()
        try {
            buryItem();
        } catch (e) {
            console.error(`SEMI: Unable to bury ${items[itemID].name} due to an error in buryItem():`);
            console.error(e);
            return false;
        } finally {
            selectedBankItem = _selectedBankItem;
            buryItemQty = _buryItemQty;
            // @ts-expect-error
            selectBankItem = _selectBankItem;
            unselectItemIfNotInBank(itemID);
        }
        return true;
    };

    const openItemWithoutConfirmation = (itemID, qty = 1) => {
        if (!checkBankForItem(itemID)) {
            return false;
        }

        const _selectedBankItem = selectedBankItem;
        selectedBankItem = itemID;
        const _openItemQty = openItemQty;
        openItemQty = qty;
        // @ts-expect-error
        const _swalFire = Swal.fire;
        // @ts-expect-error
        Swal.fire = () => {}; // Temporarily replace swal.fire() to prevent the chest popup
        try {
            openBankItem();
            if (sellItemMode) {
                toggleSellItemMode();
            }
            if (moveItemMode) {
                toggleMoveItemMode();
            }
        } catch (e) {
            console.error(`SEMI: Unable to open ${items[itemID].name} due to an error in openBankItem():`);
            console.error(e);
            return false;
        } finally {
            selectedBankItem = _selectedBankItem;
            openItemQty = _openItemQty;
            // @ts-expect-error
            Swal.fire = _swalFire;
            unselectItemIfNotInBank(itemID);
        }
        return true;
    };

    const equipSwapConfig = {
        Helmet: {
            slotID: 0,
            swapped: false,
        },
        Platebody: {
            slotID: 1,
            swapped: false,
        },
        Platelegs: {
            slotID: 2,
            swapped: false,
        },
        Boots: {
            slotID: 3,
            swapped: false,
        },
        Weapon: {
            slotID: 4,
            swapped: false,
        },
        Shield: {
            slotID: 5,
            swapped: false,
        },
        Amulet: {
            slotID: 6,
            swapped: false,
        },
        Ring: {
            slotID: 7,
            swapped: false,
        },
        Gloves: {
            slotID: 8,
            swapped: false,
        },
        Quiver: {
            slotID: 9,
            swapped: false,
        },
        Cape: {
            slotID: 10,
            swapped: false,
        },
        script: '',
    };
    /**
     * Equips an item, remembers original item in slot, and can be called again to re-equip original item.
     * @param {number} idSwap
     * @param {string} slotName
     */
    const equipSwap = (idSwap, slotName) => {
        const currentlyEquippedItemID = currentEquipmentInSlot(slotName);
        if (!equipSwapConfig[slotName].swapped) {
            equipSwapConfig[slotName].originalID = currentlyEquippedItemID;
            if (slotName === 'Weapon') {
                const currentlyEquippedShield = currentEquipmentInSlot('Shield');
                // @ts-expect-error
                equipSwapConfig['Shield'].originalID = currentlyEquippedShield;

                // Store ammo count for javelins and throwing knives
                if (items[currentlyEquippedItemID]?.ammoType === 2 || items[currentlyEquippedItemID]?.ammoType === 3) {
                    // equipSwapConfig[slotName].ammo = equipmentSets[selectedEquipmentSet].ammo;
                    equipSwapConfig[slotName].ammo = player.equipment.slots['Quiver'].quantity;
                } else {
                    equipSwapConfig[slotName].ammo = 0;
                }
            }
        }
        if (equipSwapConfig[slotName].swapped) {
            equipFromBank(equipSwapConfig[slotName].originalID, equipSwapConfig[slotName].ammo || 1);
            if (slotName === 'Weapon') {
                // @ts-expect-error
                equipFromBank(equipSwapConfig['Shield'].originalID);
            }
        } else {
            equipFromBank(idSwap);
        }
        equipSwapConfig[slotName].swapped = !equipSwapConfig[slotName].swapped;
    };

    /**
     * Returns the current level of a skill
     * @param {SkillName} skillName
     * @param {boolean} [virtualLevel=false] Whether it should return the virtual level if it's greater than 99
     */
    const currentLevel = (skillName, virtualLevel = false) => {
        if (virtualLevel) {
            return Math.max(
                skillLevel[CONSTANTS.skill[skillName]],
                exp.xp_to_level(skillXP[CONSTANTS.skill[skillName]]) - 1
            );
        } else {
            return skillLevel[CONSTANTS.skill[skillName]];
        }
    };

    /** @param {number} skillId */
    const currentLevelById = (skillId) => skillLevel[skillId];

    /** @param {SkillName} skillName */
    const currentXP = (skillName) => skillXP[CONSTANTS.skill[skillName]];

    /** @param {SkillName} skillName */
    const isMaxLevel = (skillName) => currentLevel(skillName) >= 99;

    /** @param {number} skillId */
    const isMaxLevelById = (skillId) => currentLevelById(skillId) >= 99;

    /** @param {SkillName} skillName */
    const ownsCape = (skillName) =>
        isMaxLevel(skillName) && checkBankForItem(CONSTANTS.item[`${skillName}_Skillcape`]);

    /** @returns {number} game returns item ID of current equipped cape, or -1 if none */
    // @ts-ignore
    const equippedCapeId = () => player.manager.player.equipment.slots.Cape.item.id;

    /** @param {SkillName} skillName */
    const hasCapeOn = (skillName) =>
        equippedCapeId() == CONSTANTS.item[`${skillName}_Skillcape`] ||
        equippedCapeId() == CONSTANTS.item.Max_Skillcape ||
        equippedCapeId() == CONSTANTS.item.Cape_of_Completion;

    const formatTimeFromMinutes = (min = 0) => {
        if (min == 0 || min == Infinity) {
            return '...';
        }
        let hrs = min / 60;
        let days = hrs / 24;
        if (min < 60) {
            return `${min.toFixed(1)} min`;
        } else if (min < 1440) {
            return `${hrs.toFixed(1)} hrs`;
        } else if (min >= 1440) {
            return `${days.toFixed(2)} days`;
        }
    };

    const currentSkillId = () => (currentSkillName() == '' ? -1 : CONSTANTS.skill[currentSkillName()]);

    const currentSkillName = () => {
        if (currentlyCutting == 1 || currentlyCutting == 2) {
            return 'Woodcutting';
        }
        if (isFishing) {
            return 'Fishing';
        }
        if (isBurning) {
            return 'Firemaking';
        }
        if (isCooking) {
            return 'Cooking';
        }
        if (isMining) {
            return 'Mining';
        }
        if (isSmithing) {
            return 'Smithing';
        }
        if (game.activeSkill === globalThis.ActiveSkills.THIEVING) {
            return 'Thieving';
        }
        if (isFletching) {
            return 'Fletching';
        }
        if (isCrafting) {
            return 'Crafting';
        }
        if (isRunecrafting) {
            return 'Runecrafting';
        }
        if (isHerblore) {
            return 'Herblore';
        }
        // @ts-ignore
        if (player.manager.isInCombat) {
            return 'Hitpoints';
        }
        if (isAgility) {
            return 'Agility';
        }
        if (isMagic) {
            return 'Alt Magic';
        }
        if (isSummoning) {
            return 'Summoning';
        }
        return '';
    };

    const currentCombatSkillName = () => {
        // @ts-ignore
        return player.attackStyle?.name;
    };

    const currentCombatSkillId = () => {
        // @ts-ignore
        return player.attackStyle?.id;
    };

    /** @param {SkillName} skillName */
    const stopSkill = (skillName) => {
        if (currentSkillName() !== skillName) {
            return;
        }
        switch (skillName) {
            case 'Mining':
                return mineRock(currentRock, true);
            case 'Cooking':
                return startCooking(0, false);
            case 'Smithing':
                return startSmithing(true);
            case 'Hitpoints':
                // @ts-ignore
                return player.manager.stopCombat(true);
            case 'Runecrafting':
                return selectRunecraft(0);
            case 'Summoning':
                return selectSummon(0);
        }
    };

    /** @param {string} nameOrId */
    const isCurrentSkill = (nameOrId) =>
        typeof nameOrId === 'string' ? currentSkillName() === nameOrId : currentSkillId() === nameOrId;

    /** @param {string} skill */
    const _skillImg = (skill) => `assets/media/skills/${skill}/${skill}.svg`;

    /** @param {string} skill */
    const skillImg = (skill) => _skillImg(skill.toLowerCase());

    const pages = [
        'Woodcutting',
        'Shop',
        'Bank',
        'Settings',
        'Changelog',
        'Milestones',
        'Statistics',
        'Fishing',
        'Firemaking',
        'Cooking',
        'Mining',
        'Smithing',
        'Mastery',
        'Combat',
        'Thieving',
        'Farming',
        'Fletching',
        'Crafting',
        'Runecrafting',
        'Herblore',
        'Archaeology',
        'Easter',
    ];
    const currentPageName = () => pages[currentPage];

    /** @param {string} name */
    const _changePage = (name) => {
        changePage(pages.indexOf(name));
    };

    const filterItems = (f) =>
        items
            .map((item, i) => ({ ...item, i }))
            .filter(f)
            .map(({ i }) => i);

    /** @param {number} n */
    const confirmAndCloseModal = (n = 100) => {
        setTimeout(() => {
            if (document.getElementsByClassName('swal2-confirm').length == 0) return;
            (document.getElementsByClassName('swal2-confirm')[0] as HTMLElement).click();
        }, n);
    };

    const maxHP = () => {
        return player.stats.maxHitpoints;
    };
    const currentHP = () => {
        return player.hitpoints;
    };

    const maxHitOfCurrentEnemy = () => {
        // @ts-ignore
        return combatManager.enemy.stats.maxHit;
    };

    const playerIsStunned = () => {
        return player.stun.turns > 0;
    };

    const playerIsAsleep = () => {
        return player.sleep.turns > 0;
    };

    const enemeyNextAttackCanStun = () => {
        // @ts-ignore
        return combatManager.enemy.nextAttack.onhitEffects?.[0]?.type === 'Stun';
    };

    const adjustedMaxHit = () => {
        let maxHit = SEMIUtils.maxHitOfCurrentEnemy();

        // @ts-ignore
        const { enemy } = player.manager;
        let adjustedMaxHit = enemy.getAttackMaxDamage(enemy.nextAttack);

        if (playerIsAsleep()) {
            adjustedMaxHit *= 1.2;
        }

        if (playerIsStunned()) {
            adjustedMaxHit *= 1.2;
        }

        adjustedMaxHit +=
            // @ts-expect-error
            (player.activeDOTs.size > 0 ? Math.floor(SEMIUtils.maxHP() * 0.02) : 0) +
            combatManager.enemy.modifiers.increasedFlatReflectDamage;

        return Math.ceil(adjustedMaxHit);
    };

    // @ts-expect-error
    const currentFood = () => player.food.slots[player.food.selectedSlot];

    // Broken as of v0.21
    function getItemTooltip(itemId?) {
        let potionCharges = '';
        let description = '';
        let spec = '';
        let bankClassPopoverBorder = '';
        let bankClassImg = '';
        let bankClassPopover = '';
        let bankClassQty = '';
        let bankMinWidth = '';
        let bankQtyPositioning = '';
        let hp = '';
        let bankLocked = '';
        let bankContainer = '';
        if (items[itemId].isPotion)
            potionCharges =
                "<small class='text-warning'>" + items[itemId].potionCharges + ' Potion Charges</small><br>';
        if (items[itemId].description !== undefined)
            description = "<small class='text-info'>" + items[itemId].description + '</small><br>';
        if (items[itemId].hasSpecialAttack)
            spec =
                "<small class='text-success'>SPECIAL ATTACK<br><span class='text-danger'>" +
                playerSpecialAttacks[items[itemId].specialAttackID].name +
                ' (' +
                playerSpecialAttacks[items[itemId].specialAttackID].chance +
                "%): </span><span class='text-warning'>" +
                playerSpecialAttacks[items[itemId].specialAttackID].description +
                '</span></small> ';
        if (items[itemId].canEat)
            hp =
                "<img class='skill-icon-xs ml-2' src='assets/media/skills/hitpoints/hitpoints.svg'> <span class='text-success'>+" +
                getFoodHealValue(itemId) +
                '</span>';
        let bankTooltip =
            "<div class='text-center'><div class='media d-flex align-items-center push'><div class='mr-3'><img class='bank-img m-1' src='" +
            items[itemId].media +
            "'></div><div class='media-body'><div class='font-w600'>" +
            items[itemId].name +
            '</div>' +
            potionCharges +
            description +
            spec +
            "<div class='font-size-sm'><img class='skill-icon-xs' src='assets/media/main/coins.svg'> " +
            items[itemId].sellsFor +
            hp +
            '<br></div></div></div></div>';

        return bankTooltip;
    }

    const getCharacter = () => {
        return currentCharacter;
    };

    // Adds checks to scripts in adventure mode so that scripts do not attempt to do stuff they cannot feasibly do
    const isSkillUnlocked = (skillName) => {
        return currentGamemode !== CONSTANTS.gamemode.Adventure || skillsUnlocked[CONSTANTS.skill[skillName]];
    };

    let _utilsReady = false;
    const utilsReady = () => {
        return _utilsReady;
    };

    const loadUtils = () => {
        // @ts-expect-error need to find a way to declare SEMI globally
        if (!isLoaded || typeof SEMI === 'undefined') {
            return;
        }
        clearInterval(utilLoader);

        console.log('Game Loaded. SEMIUtils ready.');

        // ToDo: Convert this variable into an event-bus event
        _utilsReady = true;
        $('body').append('<div id="SEMI-canary"></div>');
    };

    const utilLoader = setInterval(loadUtils, 50);

    return {
        utilsReady,
        changePage: _changePage,
        currentPageName,
        skillImg,
        isCurrentSkill,
        stopSkill,
        currentSkillName,
        currentSkillId,
        isSkillUnlocked,
        currentEquipment,
        currentXP,
        currentEquipmentInSlot,
        currentLevel,
        formatTimeFromMinutes,
        equipFromBank,
        isMaxLevel,
        ownsCape,
        equippedCapeId,
        enemeyNextAttackCanStun,
        maxHP,
        currentFood,
        currentHP,
        equipSwap,
        equipSwapConfig,
        isBankFull,
        sellItemWithoutConfirmation,
        buryItemWithoutConfirmation,
        openItemWithoutConfirmation,
        hasCapeOn,
        confirmAndCloseModal,
        maxHitOfCurrentEnemy,
        adjustedMaxHit,
        playerIsStunned,
        playerIsAsleep,
        getCharacter,
        customNotify,
        getElements,
        getElement,
        getBankQty,
        getBankId,
        iconSrc,
        mergeOnto,
        processItemSaleWithoutBank,
        currentCombatSkillName,
        currentLevelById,
        currentCombatSkillId,
        getItemTooltip,
        isMaxLevelById,
    };
})();
