declare class InfoIcon {
    private container;
    private image;
    private text;
    private tooltip;
    constructor(parent: HTMLElement, media: string, pillClass: string);
    protected setImage(media: string): void;
    protected setText(text: string): void;
    protected setTooltip(content: string): void;
    static readonly media: {
        skillXP: string;
        masteryXP: string;
        poolXP: string;
        preserveChance: string;
        doublingChance: string;
        interval: string;
    };
}
declare class XPIcon extends InfoIcon {
    constructor(parent: HTMLElement, xp: number);
    setXP(xp: number): void;
    private getTooltipContent;
}
declare class IntervalIcon extends InfoIcon {
    constructor(parent: HTMLElement, interval: number);
    setInterval(interval: number): void;
}
declare class DoublingIcon extends InfoIcon {
    constructor(parent: HTMLElement, interval: number);
    setChance(chance: number): void;
}
declare class MasteryXPIcon extends InfoIcon {
    constructor(parent: HTMLElement, xp: number);
    setXP(xp: number): void;
    private getTooltipContent;
}
declare class MasteryPoolIcon extends InfoIcon {
    constructor(parent: HTMLElement, xp: number);
    setXP(xp: number): void;
    private getTooltipContent;
}
declare class StealthIcon extends InfoIcon {
    constructor(parent: HTMLElement);
    setNPC(npc: ThievingNPC): void;
    private getTooltipContent;
}
